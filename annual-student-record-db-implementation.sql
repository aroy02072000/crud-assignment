-- NOTE: To test the query segments in this file first switch to another database 
-- and then drop the 'Institution' database if exists. Then select the query segements and execute them

-- create a database named institution and use it
CREATE DATABASE Institution;
USE Institution;

-- create a schema named Foundation
CREATE SCHEMA Foundation;

-- create ClassDetails table under Foundation schema
CREATE TABLE Foundation.Classes
(
	ClassId INT IDENTITY(1, 1),
	Name VARCHAR(3) NOT NULL,
	Section CHAR(1) NOT NULL,
	RoomNumber INT NOT NULL,
	CONSTRAINT PK_Classes_ClassId PRIMARY KEY(ClassId),
	CONSTRAINT UNI_Classes_Name_Section UNIQUE(Name, Section),
	CONSTRAINT UNI_Classes_RoomNumber UNIQUE(RoomNumber),
	CONSTRAINT CHK_Classes_Name CHECK(Name IN ('I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII')),
	CONSTRAINT CHK_Classes_Section CHECK(Section IN ('A', 'B', 'C')),
	CONSTRAINT CHK_Classes_RoomNumber CHECK(RoomNumber BETWEEN 100 AND 900)
);

-- insert records into ClassDetails table
INSERT INTO Foundation.Classes(Name, Section, RoomNumber) VALUES
('IX', 'A', 201),
('IX', 'B', 202),
('X', 'A', 203);

-- check whether all the records are inserted successfully in ClassDetails table
SELECT 
	ClassId, 
	Name, 
	Section, 
	RoomNumber 
FROM Foundation.Classes;

-- create a TeacherDetails table under Foundation schema
CREATE TABLE Foundation.Teachers
(
	TeacherId INT IDENTITY(1, 1),
	FName VARCHAR(50) NOT NULL,
	LName VARCHAR(50) NOT NULL,
	DOB DATE NOT NULL,
	Gender VARCHAR(6) NOT NULL,
	CONSTRAINT PK_Teachers_TeacherId PRIMARY KEY(TeacherId),
	CONSTRAINT CHK_Teachers_Gender CHECK(Gender IN ('Male', 'Female'))
);

-- insert records into TeacherDetails table
INSERT INTO Foundation.Teachers(FName, LName, DOB, Gender) VALUES
('Lisa', 'Kudrow', '1985/06/08','Female'),
('Monica', 'Bing', '1982/03/06','Female'),
('Chandler', 'Bing', '1978/12/17','Male'),
('Ross', 'Geller', '1993/01/26','Male'),
('Alex', 'Gardener', '1993/02/07','Female');

-- check whether all the records are inserted successfully in TeacherDetails table
SELECT 
	TeacherId,
	FName,
	LName,
	DOB,
	Gender
FROM Foundation.Teachers;

-- create a StudentDetails table under Foundation schema
CREATE TABLE Foundation.Students
(
	StudentId INT IDENTITY(1, 1),
	FName VARCHAR(50) NOT NULL,
	LName VARCHAR(50) NOT NULL,
	DOB DATE NOT NULL,
	Gender VARCHAR(6) NOT NULL,
	ClassId INT,
	CONSTRAINT PK_Students_StudentId PRIMARY KEY(StudentId),
	CONSTRAINT CHK_Students_Gender CHECK(Gender IN ('Male', 'Female')),
	CONSTRAINT FK_Students_ClassId FOREIGN KEY(ClassId) 
	REFERENCES Foundation.Classes(ClassId)
);

-- insert records into StudentDetails table
INSERT INTO Foundation.Students(FName, LName, DOB, Gender, ClassId) VALUES
('Scotty', 'Loman', '2006/01/31', 'Male', 1),
('Adam', 'Scott', '2005/06/01', 'Male', 1),
('Natosha', 'Beckles', '2005/01/23', 'Female', 2),
('Lilly', 'Page', '2006/11/26', 'Female', 2),
('John', 'Freeman', '2006/06/14', 'Male', 2),
('Morgan', 'Scott', '2005/05/18', 'Male', 3),
('Codi', 'Gass', '2005/12/24', 'Female', 3),
('Nick', 'Roll', '2005/12/24', 'Male', 3),
('Dave', 'Grohi', '2005/02/12', 'Male', 3),
('George', 'Steve', '2005/02/19', 'Male', 1);

-- check whether all the records are inserted successfully in StudentDetails table
SELECT
	StudentId,
	FName,
	LName,
	DOB,
	Gender,
	ClassId
FROM Foundation.Students;

-- create a TeacherAllocations table under Foundation schema
CREATE TABLE Foundation.Classes_Teachers
(
	TeacherId INT,
	ClassId INT,
	CONSTRAINT FK_Classes_Teachers_TeacherId FOREIGN KEY(TeacherId)
	REFERENCES Foundation.Teachers(TeacherId),
	CONSTRAINT FK_Classes_Teachers_ClassId FOREIGN KEY(ClassId)
	REFERENCES Foundation.Classes(ClassId)
);

-- insert record into TeacherAllocations table
INSERT INTO Foundation.Classes_Teachers(TeacherId, ClassId) VALUES
(1, 1),
(1, 2),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(5, 2);

-- check whether all the records are inserted successfully in TeacherAllocations table
SELECT
	TeacherId,
	ClassId
FROM Foundation.Classes_Teachers;

-- add CreatedAt column in all the tables under Foundation schema
ALTER TABLE Foundation.Classes
ADD 
CreatedAt DATE DEFAULT CAST(GETDATE() AS DATE) NOT NULL,
UpdatedAt DATE DEFAULT NULL; 

ALTER TABLE Foundation.Teachers
ADD 
CreatedAt DATE DEFAULT CAST(GETDATE() AS DATE) NOT NULL,
UpdatedAt DATE DEFAULT NULL; 

ALTER TABLE Foundation.Students
ADD 
CreatedAt DATE DEFAULT CAST(GETDATE() AS DATE) NOT NULL,
UpdatedAt DATE DEFAULT NULL; 

ALTER TABLE Foundation.Classes_Teachers
ADD 
CreatedAt DATE DEFAULT CAST(GETDATE() AS DATE) NOT NULL,
UpdatedAt DATE DEFAULT NULL; 

-- check whether all the tables have CreatedAt and UpdatedAt column 
SELECT 
	ClassId, 
	Name, 
	Section, 
	RoomNumber,
	CreatedAt,
	UpdatedAt
FROM Foundation.Classes;

SELECT 
	TeacherId,
	FName,
	LName,
	DOB,
	Gender,
	CreatedAt,
	UpdatedAt
FROM Foundation.Teachers;

SELECT
	StudentId,
	FName,
	LName,
	DOB,
	Gender,
	ClassId,
	CreatedAt,
	UpdatedAt
FROM Foundation.Students;

SELECT
	TeacherId,
	ClassId,
	CreatedAt,
	UpdatedAt
FROM Foundation.Classes_Teachers;

-- add a IsScholar bool column in StudentDetails table with the explicit default value as false
ALTER TABLE Foundation.Students
ADD 
IsScholar BIT DEFAULT 0 NOT NULL;

-- add a IsScholar bool column in TeacherDetails table with the explicit default value as false
ALTER TABLE Foundation.Teachers
ADD 
IsContracted BIT DEFAULT 0 NOT NULL;

-- check whehter StudentDetails and TeacherDetails table have IsScholar and IsContracted column
SELECT
	StudentId,
	FName,
	LName,
	DOB,
	Gender,
	ClassId,
	CreatedAt,
	UpdatedAt,
	CASE 
		WHEN IsScholar=0 THEN 'false'
		ELSE 'true'
	END AS IsScholar
FROM Foundation.Students;

SELECT 
	TeacherId,
	FName,
	LName,
	DOB,
	Gender,
	CreatedAt,
	UpdatedAt,
	CASE 
		WHEN IsContracted=0 THEN 'false'
		ELSE 'true'
	END AS IsContracted
FROM Foundation.Teachers;

-- update IsScholar to true where student name contains Scott in their names
UPDATE Foundation.Students 
SET IsScholar=1,
	UpdatedAt=CAST(GETDATE() AS DATE)
WHERE CHARINDEX('Scott', FName + ' ' + LName) > 0;

-- check whether StudentDetails table is updated
SELECT
	StudentId,
	FName,
	LName,
	DOB,
	Gender,
	ClassId,
	CreatedAt,
	UpdatedAt,
	CASE 
		WHEN IsScholar=0 THEN 'false'
		ELSE 'true'
	END AS IsScholar
FROM Foundation.Students;

-- update IsContracted to true for the teachers of class IX-B
UPDATE Foundation.Teachers
SET IsContracted=1,
	UpdatedAt=CAST(GETDATE() AS DATE)
WHERE TeacherId 
IN (SELECT CT.TeacherId FROM Foundation.Classes_Teachers AS CT
LEFT OUTER JOIN Foundation.Classes AS CL
ON CT.ClassId=CL.ClassId
WHERE CL.Name='IX' 
AND Section='B');

-- check whether TeacherDetails table is updated
SELECT 
	TeacherId,
	FName,
	LName,
	DOB,
	Gender,
	CreatedAt,
	UpdatedAt,
	CASE 
		WHEN IsContracted=0 THEN 'false'
		ELSE 'true'
	END AS IsContracted
FROM Foundation.Teachers;

-- remove record of student whose name is George Steve
DELETE FROM Foundation.Students
WHERE FName='George' 
AND LName='Steve';

-- check whether the record is removed from the StudentDetails table
SELECT
	StudentId,
	FName,
	LName,
	DOB,
	Gender,
	ClassId,
	CreatedAt,
	UpdatedAt,
	CASE 
		WHEN IsScholar=0 THEN 'false'
		ELSE 'true'
	END AS IsScholar
FROM Foundation.Students;

--remove record of teacher whose name is Alex Gardener
BEGIN
	DECLARE @intTeacherId INT;
	SET @intTeacherId = (SELECT TeacherId FROM Foundation.Teachers
	WHERE FName='Alex' 
	AND LName='Gardener');

	-- first, remove the record from TeacherAllocations table 
	-- as it has a foreign key which points to the TeacherDetails table
	DELETE FROM Foundation.Classes_Teachers
	WHERE TeacherId=@intTeacherId;

	-- now can delete the record from the master table that is TeacherDetails
	DELETE FROM Foundation.Teachers
	WHERE TeacherId=@intTeacherId;
END

-- check whether the record is removed from TeacherDetails and TeacherAllocations table
SELECT
	TeacherId,
	ClassId,
	CreatedAt,
	UpdatedAt
FROM Foundation.Classes_Teachers;

SELECT 
	TeacherId,
	FName,
	LName,
	DOB,
	Gender,
	CreatedAt,
	UpdatedAt,
	CASE 
		WHEN IsContracted=0 THEN 'false'
		ELSE 'true'
	END AS IsContracted
FROM Foundation.Teachers;